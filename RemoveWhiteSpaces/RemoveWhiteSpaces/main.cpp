#include <iostream>
#include <string.h>

using namespace std;

class Engine
{
    public:
        string removeWhiteSpacesFromString(string str)
        {
            int len   = (int)str.length();
            int count = 0;
            int i     = 0;
            for(i = 0 ; i < len ; i++)
            {
                if(str[i] == ' ')
                {
                    count++;
                }
                else
                {
                    str[i-count] = str[i];
                }
            }
            for(i = i - count ; i < len ; i++)
            {
                str[i] = '\0';
            }
            return str;
        }
};

int main(int argc, const char * argv[])
{
    string str = "  Hi, How are you?  ";
    Engine e   = Engine();
    str        = e.removeWhiteSpacesFromString(str);
    cout<<str<<endl;
    return 0;
}
